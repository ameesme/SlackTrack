# SlackTrack
Tracks when your Slack-contacts are online.

## Disclaimer / Warning
Using this tool without people's permissions might or might not be a violation of their privacy. Please be responsible while using this tool by warning your contacts and allowing them to opt-out of being tracked. Usage is obviously at your own risk.

## How to use

1. Create a `config.js` file and add a Slack access-token
2. Ask people for permission to log this information
3. Run `node index.js`
