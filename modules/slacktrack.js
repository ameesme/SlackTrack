// Modules
const tracker = require(__dirname + '/tracker.js');
const storage = require(__dirname + '/storage.js');

function cycle() {
	tracker.track(storage.store);
}

cycle();