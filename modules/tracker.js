// External dependencies
const Slack = require('slack-node');

// Variables
const config = require(__dirname + '/../.config.js');

// Instances
var slack = new Slack(config.token);

function track(callBack) {
	const currentDate = new Date();
	slack.api('users.list', {presence: 1}, function(error, response) {
		if(error) {
			console.log(`${currentDate} [TRACK] Failed to get users`);
			console.log(error);
			callBack(false);
		}
		console.log(`${currentDate} [TRACK] Got ${response.members.length} users`);
		callBack(response.members);
	});
}

module.exports = {
	track
}