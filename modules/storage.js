// External dependencies
const fs = require('fs');
const nedb = require('nedb');

// Variables
const optout = fs.readFileSync(__dirname + '/../.optout', 'utf-8').split('\n');
const optoutUserNames = optout.filter((record) => {
	const recordIsEmpty = record === '';
	const recordIsComment = record.indexOf('#') > -1;

	return !(recordIsEmpty || recordIsEmpty);
});

// Instances
const dataStore = new nedb({ filename: __dirname + '/../dataStore.db', autoload: true });

// Private functions
function filterUsers(users) {
	return users.filter((user) => optoutUserNames.indexOf(user.name) === -1);
}

// Public functions
function store(users, callBack) {
	const filteredUsers = filterUsers(users);

	// Remove unneeded metadata

	dataStore.insert({
		timestamp: new Date().getTime(),
		users: filteredUsers
	}, function (error, newDocs) {
		const currentDate = new Date();
		if (error) {
			console.log(`${currentDate} [STORE] Failed to store users`);
			console.log(error);
			callBack ? callBack(false) : null;
			return;
		}
		console.log(`${currentDate} [STORE] Stored ${filteredUsers.length} users`);
		callBack ? callBack(newDocs) : null;
	});
}

function read(callBack) {
	dataStore.find({}, callBack);
}

function readTimerange(timeStart, timeEnd, callBack) {
	dataStore.find({
		timestamp: {
			$gt: timeStart,
			$lt: timeEnd
		}
	}, callBack);
}

// Export
module.exports = {
	store,
	read,
	readTimerange
};
